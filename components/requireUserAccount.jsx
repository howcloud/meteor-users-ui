/**
 * 
 */

/** 

Require User Account
Shows a login/signup prompt for a certain action
Once the login/signup is complete the action is completed

**/

var _requireUserAccount;

requireUserAccount = function (options) {
    options = options || {};

    var user = Meteor.user();
    if (user && !user.isGuest()) {
       if (options.callback) options.callback();
       return;
    }

    analyticsEvent('requireUserAccount', {
        action: options.action
    });

    return delegateRender(_.extend({

        /** Render **/

        render: function () {
            return (
                <LoginModal action={options.action} defaultMode={options.mode} defaultName={options.defaultName} defaultEmail={options.defaultEmail} onCancel={this.cancel} onLogin={this.onLogin} />
            );
        },

        /** UI Callbacks **/

        onLogin: function () {
            this.cancel();
            if (options.callback) options.callback();
        },

    }, options));
}
