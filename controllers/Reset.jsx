/**
 * 
 */

var ResetPassword = React.createClass({

	/** Component Properties **/

	propTypes: {
		user_id: React.PropTypes.string, // not actually used
		token: React.PropTypes.string,
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin
	],

	/** State **/

	getInitialState: function () {
		return {
			
		}
	},

	/** Dimensions **/

	refreshDimensions: function () {
		var $window = jQuery(window);

		this.setState({
			windowHeight: $window.height(),
			windowWidth: $window.width()
		});
	},

	/** React **/

	componentDidMount: function () {
		
		/** Resize **/

		this.refreshDimensions();
		window.addEventListener('resize', this.refreshDimensions);

	},
	componentWillUnmount: function () {

		/** Resize **/

		window.removeEventListener('resize', this.refreshDimensions);

	},

	/** Render **/

	render: function () {
		return (
			<div className="container">

				<Header fixedWidth={true} fixed={false} transparent={true}>
					<Header_Content_LogoLeft />
				</Header>

				<Splash main={true} innerBackgroundColor="#082551" valign="middle" minHeight={this.state.windowHeight} style={{marginTop: -42}}>
					
					<div className="inner-container fixed-width">
						<div className="content-pad">
							<div style={{maxWidth: 500, margin: '0 auto'}}>
								<div className="content-box content-box-decorate loginForm-container">
									<div className="content-bigpad">
										<h2 style={{marginBottom: 20}}>
											Create a new password
										</h2>

										<TextInput ref="password" placeholder="Create a new password" password={true} disabled={this.state.loading} onEnterUp={this.resetPassword_submit} style={{marginBottom: 10}} />
										<TextInput ref="confirmPassword" placeholder="Re-enter password" password={true} disabled={this.state.loading} onEnterUp={this.resetPassword_submit} style={{marginBottom: 10}} />

										<Button disabled={this.state.loading} onClick={this.resetPassword_submit} size="large">Create New Password</Button>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</Splash>
			</div>
		);
	},

	/** Callbacks **/

	resetPassword_submit: function () {
		var _password = this.refs.password.value();
		if (!_password) {
			alert('Please enter your password');
			return;
		}

		var _confirmPassword = this.refs.confirmPassword.value();
		if (!_confirmPassword) {
			alert('Please confirm your password by entering it again');
			return;
		} else if (_confirmPassword !== _password) {
			alert('Your passwords do not match - please enter them again');
			return;
		}

		/* Do Reset */
		// The way we have manually constructed reset tokens is the same as how Meteor does it internally so we can still use the main accounts function to do the actual password reset
		// But it is a bit shit - there are some edge cases which they just don't care about (users with the same token) and deal with in odd ways vs the obvious way (hence why I originally have user_id in the props for thie controller and reset links)

		this.setState({loading: true});
		Accounts.resetPassword(this.props.token, _password, function (error, result) {
			this.setState({loading: false});

			if (error) {
				if (error.reason) {
					alert(error.reason);
				} else {
					alert("There was a problem logging you in, please try again");
				}

				return;
			}

			alert("Your password has been updated and we have logged you into HowCloud");
			if (typeof Iron !== 'undefined') Iron.Location.go("/my/");
		}.bind(this));
	},

});

if (typeof Router !== 'undefined') {
	Router.route({
		path: '/reset/:user_id/:token',
		controllerComponent: ResetPassword
	});
}
