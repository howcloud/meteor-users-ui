Package.describe({
	name: 'howcloud:users-ui',
	version: '0.1.0',
	summary: 'Exports web UI components related to user accounts',
});

Package.on_use(function (api) {

	api.use('howcloud:react');
	api.use('howcloud:react-deps');
	api.use('howcloud:react-build');
	api.use('howcloud:ui-base');
	api.use('howcloud:router');
	api.use('howcloud:users');

	// Components

	var componentPath = 'components/';
	api.add_files(componentPath + 'LoginForm.jsx', ['client', 'server']);
	api.add_files(componentPath + 'LoginModal.jsx', ['client', 'server']);
	api.add_files(componentPath + 'requireUserAccount.jsx', ['client']);
	api.add_files(componentPath + 'RequireNotGuest.jsx', ['client', 'server']);

	api.export('LoginForm', ['client', 'server']);
	api.export('LoginModal', ['client', 'server']);
	api.export('RequireNotGuest', ['client', 'server']);
	api.export('requireUserAccount', ['client']);

	// Controllers
	// NOTE: These controllers make assumptions about headers etc available
	// TODO: abstract away from this (platform would not work with this, for example)

	var controllerPath = 'controllers/';
	api.add_files(controllerPath + 'Login.jsx', ['client', 'server']);
	api.add_files(controllerPath + 'Reset.jsx', ['client', 'server']);

	api.export('Login', ['client', 'server']);

});